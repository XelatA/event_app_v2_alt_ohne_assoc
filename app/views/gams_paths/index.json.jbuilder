json.array!(@gams_paths) do |gams_path|
  json.extract! gams_path, :id, :gams_path_url
  json.url gams_path_url(gams_path, format: :json)
end
