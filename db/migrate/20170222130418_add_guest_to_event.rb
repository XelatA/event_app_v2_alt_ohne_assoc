class AddGuestToEvent < ActiveRecord::Migration
  def change
    add_column :events, :guest, :integer
  end
end
