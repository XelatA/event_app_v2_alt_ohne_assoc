class AddTimeToPeriods < ActiveRecord::Migration
  def change
    add_column :periods, :time, :date
  end
end
